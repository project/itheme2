<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
	<!--[if lt IE 7]>
	<link rel="stylesheet" href="ie-gif.css" type="text/css" />
	<![endif]-->
   
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="page">
	<div id="wrapper">
	<div id="header">
		<a href="<?php print check_url($base_path); ?>"><?php if ($logo) { print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />'; } ?></a>	
		<h2><a href="<?php print check_url($base_path); ?>"><?php print check_plain($site_name); ?></a></h2>
		<div class="slogan"><?php print check_plain($site_slogan); ?></div>	
		<?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>	
    </div><!-- /header -->
    <div id="left-col">
        <div id="nav">              
            <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
		</div><!-- /nav -->
		<div id="content">
			<?php if ($mission != ""): ?>
			<div class="mission"><?php print $mission ?></div>
			<?php endif; ?>
			<?php print $header; ?>			
			<?php if ($title != ""): ?>
			<h1><?php print $title ?></h1>
			<?php endif; ?>			
			<?php if ($tabs != ""): ?>
			<?php print $tabs ?>
			<?php endif; ?>						
			<?php if ($help != ""): ?>
			<p id="help"><?php print $help ?></p>
			<?php endif; ?>
			<?php if ($messages != ""): ?>
			<div id="message"><?php print $messages ?></div>
			<?php endif; ?>
			<?php print($content) ?>		</div><!--/content -->
		<div id="footer"><?php print $footer_message ?> Powered by <strong>Drupal</strong> <a href="http://www.salle.ru">iTheme</a></div>
    </div><!--/left-col -->		
	<div id="sidebar"  >
	 <?php if ($left): ?>
        <div id="sidebar-left" class="sidebar">
                    <?php print $left ?>        </div>
      <?php endif; ?>
	  <?php if ($sidebar_right): ?>
          <?php print $sidebar_right ?>
      <?php endif; ?> 
	</div><!--/sidebar -->
    <hr class="hidden" />
</div>
	<!--/wrapper -->
</div><!--/page -->
<?php print $closure ?>
</body>
</html>